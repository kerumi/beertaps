import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  increment,
  decrement,
  calcBeerOrder,
  changeBeerTapValue
} from '../../modules/tapper'

import {
  Grid,
  Header,
  Dimmer,
  Icon,
  Message,
  Segment,
  Button,
  Divider,
  List,
  Input
} from 'semantic-ui-react'

class Tapper extends Component {
  handleChange = ({ target: { value } }) => {
    const { changeBeerTapValue } = this.props
    const number = parseInt(value)
    if (number >= 1 && number <= 1000) {
      changeBeerTapValue(number)
    } else changeBeerTapValue('')
  }

  render() {
    const { beerTap, beerOrder, calcInit, decrement, increment } = this.props
    return (
      <Grid
        textAlign="center"
        padded
        style={{ height: '100vh' }}
        verticalAlign="middle">
        <Grid.Column style={{ maxHeight: '85%', maxWidth: 600 }}>
          <Header className="main-header" color="yellow" textAlign="center">
            <Icon name="beer" /> BeerTaps
          </Header>

          <Message className="description">
            There are 1k <em>BeerTaps</em> ordered from 1 to 1000 <br /> <br />{' '}
            Legend says that the best beers are the ones with the taps whose
            numbers matches this conditions:
          </Message>

          <Segment placeholder>
            <Grid columns={2} textAlign="center">
              <Divider vertical>And</Divider>
              <Grid.Row verticalAlign="middle">
                <Grid.Column>
                  <Dimmer.Dimmable
                    as={Segment}
                    inverted
                    style={{ background: 'transparent', padding: '0' }}
                    dimmed>
                    <p style={{ color: '#fefefe' }}>
                      The sum of the divisors is greater than it's number
                    </p>
                    {calcInit ? (
                      beerOrder.sumIsGreater ? (
                        <Dimmer
                          active={true}
                          style={{ background: 'transparent' }}>
                          <Header as="h4" icon color="yellow" inverted>
                            <Icon name="check" />
                          </Header>
                        </Dimmer>
                      ) : (
                        <Dimmer
                          active={true}
                          style={{ background: 'transparent' }}>
                          <Header as="h4" icon color="red" inverted>
                            <Icon name="x" />
                          </Header>
                        </Dimmer>
                      )
                    ) : (
                      <div />
                    )}
                  </Dimmer.Dimmable>
                </Grid.Column>
                <Grid.Column>
                  <Dimmer.Dimmable
                    as={Segment}
                    inverted
                    style={{ background: 'transparent', padding: '0' }}
                    dimmed>
                    <p style={{ color: '#fefefe' }}>
                      The sum of the divisors is not equal to it's number
                    </p>
                    {calcInit ? (
                      beerOrder.subsetNotEqual ? (
                        <Dimmer
                          active={true}
                          style={{ background: 'transparent' }}>
                          <Header as="h4" icon color="yellow" inverted>
                            <Icon name="check" />
                          </Header>
                        </Dimmer>
                      ) : (
                        <Dimmer
                          active={true}
                          style={{ background: 'transparent' }}>
                          <Header as="h4" icon color="red" inverted>
                            <Icon name="x" />
                          </Header>
                        </Dimmer>
                      )
                    ) : (
                      <div />
                    )}
                  </Dimmer.Dimmable>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Segment>
          <h3>Waiter's coming...</h3>
          <h3>Can you order the best tap number?</h3>
          <Segment className="taps-amount" size="mini">
            <Input
              value={beerTap}
              onChange={this.handleChange}
              size="huge"
              type="number"
              action>
              <input />
              <Button onClick={decrement} compact color="yellow" icon="minus" />
              <Button onClick={increment} compact color="yellow" icon="plus" />
            </Input>
          </Segment>
          {beerOrder.divisors.length >= 1 && <h3> Divisors: </h3>}
          <List horizontal>
            {beerOrder.divisors.map((divisor, i) => (
              <List.Item key={i} className="divisor-item">
                {divisor}
              </List.Item>
            ))}
          </List>
        </Grid.Column>
      </Grid>
    )
  }
}

const mapStateToProps = ({ tapper }) => ({
  beerTap: tapper.beerTap,
  beerOrder: tapper.beerOrder,
  calcInit: tapper.calcInit,
  isIncrementing: tapper.isIncrementing,
  isDecrementing: tapper.isDecrementing,
  isPerfectBeer: tapper.isPerfectBeer,
  beerOrdered: tapper.beerOrdered
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      increment,
      decrement,
      calcBeerOrder,
      changeBeerTapValue
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tapper)
